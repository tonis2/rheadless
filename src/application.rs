use crate::DatabaseCore;
use hyper::client::HttpConnector;
use hyper::Client;

#[derive(Clone, Debug)]
pub struct Configuration {
    pub database_path: String,
    pub ip: String,
    pub port: String,
    pub database_login: Option<String>,
    pub database_password: Option<String>,
}

impl Default for Configuration {
    fn default() -> Self {
        Configuration {
            database_path: "./database.db".into(),
            ip: "127.0.0.1".into(),
            port: "1337".into(),
            database_login: None,
            database_password: None,
        }
    }
}

impl Configuration {
    pub fn get_url(&self) -> String {
        format!("{}:{}", self.ip, self.port)
    }
}

#[derive(Clone, Debug)]
pub struct Application<T: DatabaseCore + Sized> {
    pub database: T,
    pub configuration: Configuration,
    pub client: hyper::client::Client<HttpConnector>,
}

impl<T: DatabaseCore> Application<T> {
    pub fn new(configuration: Configuration, database: T) -> Self {
        Self {
            client: Client::new(),
            database,
            configuration,
        }
    }
}
