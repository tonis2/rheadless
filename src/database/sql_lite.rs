use super::{DBResult, DatabaseCore, Error};
use crate::Configuration;
use sqlite::Connection;

pub struct SqliteDatabase {
    connection: Connection,
}

impl SqliteDatabase {
    pub fn new(configuration: Configuration) -> SqliteDatabase {
        let connection = sqlite::open(configuration.database_path).unwrap();
        SqliteDatabase { connection }
    }
}

impl DatabaseCore for SqliteDatabase {
    fn save(&self, table: &String, values: Vec<String>, keys: Vec<String>) -> DBResult {
        let values_mapped: Vec<String> =
            values.iter().map(|value| format!("'{}'", value)).collect();
        let command = format!(
            "INSERT INTO {} ({}) VALUES ({})",
            table,
            keys.join(","),
            values_mapped.join(",")
        );
        let result = self.connection.execute(command);

        if let Some(e) = result.err() {
            return Err(Error::new(e.message.unwrap()));
        }

        Ok(())
    }
}
