mod sql_lite;

pub trait DatabaseCore {
    fn save(&self, table: &String, values: Vec<String>, keys: Vec<String>) -> DBResult;
}

pub use sql_lite::SqliteDatabase;

pub struct Error {
    pub message: Option<String>,
}

impl Error {
    pub fn new<T: Sized + Into<String>>(message: T) -> Self {
        Self {
            message: Some(message.into()),
        }
    }
}

pub type DBResult = Result<(), Error>;
