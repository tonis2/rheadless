use std::task::{Context, Poll};

use crate::{get_query_map, Application, DatabaseCore};
use futures_util::future;
use hyper::{header::CONTENT_TYPE, service::Service, Body, Method, Request, Response, StatusCode};
use multer::Multipart;
use std::sync::{Arc, Mutex};

pub struct BaseRoutes<T: Sized + DatabaseCore> {
    pub app: Arc<Mutex<Application<T>>>,
}

impl<T: Sized + DatabaseCore> BaseRoutes<T> {
    pub fn new(app: Arc<Mutex<Application<T>>>) -> BaseRoutes<T> {
        Self { app }
    }
}

impl<T: Sized + DatabaseCore> Service<Request<Body>> for BaseRoutes<T> {
    type Response = Response<Body>;
    type Error = hyper::Error;
    type Future = future::Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Ok(()).into()
    }

    #[tokio::main]
    async fn call(&mut self, req: Request<Body>) -> Self::Future {
        let query = req.uri().query();
        let query_map = get_query_map(query.unwrap());

        let boundary = req
            .headers()
            .get(CONTENT_TYPE)
            .and_then(|ct| ct.to_str().ok())
            .and_then(|ct| multer::parse_boundary(ct).ok());

        //Route doese not have entry type query
        if query.is_none() && query_map.get("entry").is_some() {
            return future::ok(
                Response::builder()
                    .status(StatusCode::NO_CONTENT)
                    .body(Body::empty())
                    .unwrap(),
            );
        }
        //POST but data type not form-data
        if req.method() == &Method::POST && boundary.is_none() {
            return future::ok(
                Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(Body::empty())
                    .unwrap(),
            );
        }
        match (req.method(), req.uri().path()) {
            (&Method::POST, "/api/content/create") => {
                let mut multipart = Multipart::new(req.into_body(), boundary.unwrap());
                let table = query_map.get("entry").unwrap();

                let mut values: Vec<String> = Vec::new();
                let mut keys: Vec<String> = Vec::new();
                while let Some(field) = multipart.next_field().await.unwrap() {
                    let file_name: String = field.name().unwrap().into();
                    let text: String = field.text().await.unwrap().into();
                    keys.push(file_name);
                    values.push(text)
                }

                let result = self.app.lock().unwrap().database.save(table, values, keys);
                if let Some(error) = result.err() {
                    return future::ok(
                        Response::builder()
                            .status(StatusCode::BAD_REQUEST)
                            .body(format!("Failed with {}", error.message.unwrap()).into())
                            .unwrap(),
                    );
                }
                future::ok(
                    Response::builder()
                        .status(StatusCode::OK)
                        .body(format!("Data saved").into())
                        .unwrap(),
                )
            }
            (&Method::GET, "/api/content/get") => {
                let table = query_map.get("entry").unwrap();

                // self.app.lock().unwrap().database.save(table, values, keys);
                let body = format!("Hello");

                future::ok(Response::new(body.into()))
            }
            _ => {
                return future::ok(
                    Response::builder()
                        .status(StatusCode::NO_CONTENT)
                        .body(Body::empty())
                        .unwrap(),
                )
            }
        }
    }
}
