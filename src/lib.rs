mod application;
mod database;
pub mod services;

pub use application::{Application, Configuration};
use std::collections::HashMap;

pub use database::{DatabaseCore, SqliteDatabase};

pub fn get_query_map(query: &str) -> HashMap<String, String> {
    query
        .split("&")
        .map(|string| {
            let split_values: Vec<String> =
                string.split("=").map(|value| value.to_string()).collect();
            (split_values[0].clone(), split_values[1].clone())
        })
        .collect::<HashMap<String, String>>()
}
