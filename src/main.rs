#![warn(rust_2018_idioms)]

use hyper::server::conn;
use redless::{services, Application, Configuration, SqliteDatabase};
use std::sync::{Arc, Mutex};
use tokio::net::TcpListener;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let configuration = Configuration {
        database_path: "examples/test.db".into(),
        ..Default::default()
    };

    let app = Arc::new(Mutex::new(Application::new(
        configuration.clone(),
        SqliteDatabase::new(configuration.clone()),
    )));

    let mut listener = TcpListener::bind(&configuration.get_url())
        .await
        .expect("failed to bind listener");
    loop {
        let result = listener.accept().await;
        if let Err(e) = result {
            println!("{:?}", e);
            continue;
        } else if let Ok((socket, _client_addr)) = result {
            let app = app.clone();
            tokio::spawn(async move {
                conn::Http::new()
                    .http1_only(true)
                    .serve_connection(socket, services::BaseRoutes::new(app.to_owned()))
                    .with_upgrades()
                    .await
                    .expect("failed to create connection");
            });
        }
    }
}
